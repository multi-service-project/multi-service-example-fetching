const axios = require('axios');

const nasaApiKey = process.env.NASA_API_KEY;
if (!nasaApiKey) throw new Error('NASA_API_KEY is required');

module.exports = {
  async dataForDate(date) {
    const url = `https://api.nasa.gov/planetary/apod?api_key=${nasaApiKey}&date=${date}`;
    const response = await axios.get(url);
    return response.data;
  }
};
