const redis = require('redis');
const cfenv = require('cfenv');
const path = require('path');

const expireTimeInSeconds = 4 * 60 * 60;
const appEnv = cfenv.getAppEnv({vcapFile: path.join(__dirname, '..', 'vcap-local.json')});
const serviceCreds = appEnv.getServiceCreds('p.redis');
const {host, port, password} = serviceCreds;

module.exports = {
  store(date, data) {
    const client = redis.createClient({host, port, password});

    return new Promise((resolve, reject) => {
      client.setex(date, expireTimeInSeconds, JSON.stringify(data), (err, result) => {
        err ? reject(err) : resolve();
      });
    });
  }
};
