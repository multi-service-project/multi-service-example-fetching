const express = require('express');
const fetchRoute = require('./routes/fetch');

const port = process.env.PORT || 3001;

const app = express();

app.use('/', fetchRoute);

app.listen(port, () => console.log(`'fetching' app listening on port ${port}`));
