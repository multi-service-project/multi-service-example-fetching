const express = require('express');
const router = express.Router();
const nasaClient = require('../services/nasa');
const redisClient = require('../services/redis');

router.get('/', async (req, res, next) => {
  const { date } = req.query;

  const rawData = await nasaClient.dataForDate(date);

  const data = {
    title: rawData.title,
    mediaType: rawData.media_type,
    date: rawData.date
  };

  await redisClient.store(date, data);
  await res.json(data);
});

module.exports = router;
